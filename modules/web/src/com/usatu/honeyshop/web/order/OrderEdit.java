package com.usatu.honeyshop.web.order;

import com.haulmont.cuba.gui.components.AbstractEditor;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.usatu.honeyshop.entity.Order;
import com.usatu.honeyshop.entity.OrderLine;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

public class OrderEdit extends AbstractEditor<Order> {
    @Inject
    private CollectionDatasource<OrderLine, UUID> linesDs;

    @Override
    public void init(Map<String, Object> params) {
        linesDs.addCollectionChangeListener(e -> calculateAmount());
    }

    private void calculateAmount() {
        BigDecimal amount = BigDecimal.ZERO;
        for(OrderLine orderLine : linesDs.getItems()) {
            amount = amount.add(orderLine.getHoney().getPrice().multiply(orderLine.getQuantity()));
        }
        getItem().setAmount(amount);
    }
}