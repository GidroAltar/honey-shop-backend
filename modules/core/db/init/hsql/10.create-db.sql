-- begin HONEYSHOPBACKEND_ORDER
create table HONEYSHOPBACKEND_ORDER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255),
    HONEY_PROVIDER_ID varchar(36),
    HONEY_CONSUMER_ID varchar(36),
    AMOUNT decimal(19, 2),
    DESCRIPTION varchar(350),
    START_DATE date,
    STATUS varchar(255),
    --
    primary key (ID)
)^
-- end HONEYSHOPBACKEND_ORDER
-- begin HONEYSHOPBACKEND_HONEY
create table HONEYSHOPBACKEND_HONEY (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    PRICE decimal(19, 2) not null,
    STATUS varchar(255),
    DESCRIPTION varchar(350),
    --
    primary key (ID)
)^
-- end HONEYSHOPBACKEND_HONEY
-- begin HONEYSHOPBACKEND_HONEY_PROVIDER
create table HONEYSHOPBACKEND_HONEY_PROVIDER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    TELEPHONE varchar(15),
    EMAIL varchar(255),
    DESCRIPTION longvarchar,
    --
    primary key (ID)
)^
-- end HONEYSHOPBACKEND_HONEY_PROVIDER
-- begin HONEYSHOPBACKEND_ORDER_LINE
create table HONEYSHOPBACKEND_ORDER_LINE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    ORDER_ID varchar(36),
    HONEY_ID varchar(36),
    QUANTITY decimal(19, 3),
    --
    primary key (ID)
)^
-- end HONEYSHOPBACKEND_ORDER_LINE
-- begin HONEYSHOPBACKEND_HONEY_CONSUMER
create table HONEYSHOPBACKEND_HONEY_CONSUMER (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    EMAIL varchar(255),
    TELEPHONE varchar(255),
    DESCRIPTION longvarchar,
    --
    primary key (ID)
)^
-- end HONEYSHOPBACKEND_HONEY_CONSUMER
