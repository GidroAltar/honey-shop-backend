alter table HONEYSHOPBACKEND_ORDER_LINE add constraint FK_HONEYSHOPBACKEND_ORDER_LINE_ORDER foreign key (ORDER_ID) references HONEYSHOPBACKEND_ORDER(ID);
alter table HONEYSHOPBACKEND_ORDER_LINE add constraint FK_HONEYSHOPBACKEND_ORDER_LINE_HONEY foreign key (HONEY_ID) references HONEYSHOPBACKEND_HONEY(ID);
create index IDX_HONEYSHOPBACKEND_ORDER_LINE_ORDER on HONEYSHOPBACKEND_ORDER_LINE (ORDER_ID);
create index IDX_HONEYSHOPBACKEND_ORDER_LINE_HONEY on HONEYSHOPBACKEND_ORDER_LINE (HONEY_ID);
