package com.usatu.honeyshop.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.haulmont.cuba.core.entity.StandardEntity;

@Table(name = "HONEYSHOPBACKEND_ORDER_LINE")
@Entity(name = "honeyshopbackend$OrderLine")
public class OrderLine extends StandardEntity {
    private static final long serialVersionUID = 1269491841424667490L;

    @OnDelete(DeletePolicy.CASCADE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID")
    protected Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "HONEY_ID")
    protected Honey honey;

    @Column(name = "QUANTITY", precision = 19, scale = 3)
    protected BigDecimal quantity;

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public void setHoney(Honey honey) {
        this.honey = honey;
    }

    public Honey getHoney() {
        return honey;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }


}