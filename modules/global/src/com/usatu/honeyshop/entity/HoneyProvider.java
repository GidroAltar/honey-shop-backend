package com.usatu.honeyshop.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|name")
@Table(name = "HONEYSHOPBACKEND_HONEY_PROVIDER")
@Entity(name = "honeyshopbackend$HoneyProvider")
public class HoneyProvider extends StandardEntity {
    private static final long serialVersionUID = 257751009814309461L;

    @NotNull
    @Column(name = "NAME", nullable = false)
    protected String name;

    @Column(name = "TELEPHONE", length = 15)
    protected String telephone;

    @Column(name = "EMAIL")
    protected String email;

    @Lob
    @Column(name = "DESCRIPTION")
    protected String description;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }


}